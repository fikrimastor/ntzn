<?php

namespace App\Http\Livewire;

use Exception;
use App\Services\RandomService;
use Illuminate\Validation\Rule;
use App\Services\ShortlinkService;
use Illuminate\Support\Facades\Session;
use LivewireUI\Modal\ModalComponent;

class EditShortlinkModal extends ModalComponent
{
    public $shortId;
    public $code;
    public $info;
    public $url;
    public $messageText = '';

    protected $listeners = ['shortlinkUpdated'];

    public function mount(ShortlinkService $shortlinkService)
    {
        $shortlink = $shortlinkService->shortlink($this->shortId);

        $this->code = $shortlink->code;
        $this->info = $shortlink->info;
        $this->url = $shortlink->url;
    }

    protected function rules()
    {
        return [
            'code' => ['nullable', 'string', 'max:20', Rule::unique('shortlinks', 'code')->ignore($this->shortId)],
            'info' => ['nullable', 'string', 'max:255'],
            'url' => ['required', 'url'],
        ];
    }

    public function editShortlink(ShortlinkService $shortlinkService)
    {
        $shortlink = $shortlinkService->shortlink($this->shortId);

        $validated = $this->validate();

        $validated['user_id'] = auth()->id();

        if ($this->code == null) {
            $random = new RandomService();
            $validated['code'] = $random->generateCode(8);
        }

        try {

            $shortlinkService->updateShortlink($shortlink, $validated);

            // $this->messageText = 'Shortlink updated successfully!';

        } catch (Exception $e) {
            $this->messageText = 'An error has been occured!';
        }

        $this->emitTo(ShortlinkList::class, 'pg:eventRefresh-default');

        $this->emit('closeModal');

        $this->dispatchBrowserEvent('item-updated');

        session()->flash('success', 'Shortlink updated successfully!');
    }

    public function render()
    {
        return view('livewire.edit-shortlink-modal');
    }
}
