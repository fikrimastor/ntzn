<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShortlinkController extends Controller
{
    public function getIndex()
    {
        return view('shortlink.index');
    }
}
