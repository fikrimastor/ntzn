<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Shortlink extends Model
{
    use HasFactory;

    protected $table = 'shortlinks';

    // protected $guarded = [];

    protected $fillable = [
        'code',
        'user_id',
        'info',
        'url'
    ];
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('user_id', function (Builder $builder) {
            $builder->where('user_id', '=', auth()->id());
        });
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
