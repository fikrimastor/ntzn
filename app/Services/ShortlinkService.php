<?php

namespace App\Services;

use App\Models\Shortlink;

/**
 * Class ShortlinkService.
 */
class ShortlinkService
{
    public function shortlink($id)
    {
        return Shortlink::whereId($id)->first();
    }

    public function updateShortlink(Shortlink $shortlink, $validated)
    {
        return $shortlink->update($validated);
    }

}
