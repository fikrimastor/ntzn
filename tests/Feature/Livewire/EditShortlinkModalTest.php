<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\EditShortlinkModal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class EditShortlinkModalTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(EditShortlinkModal::class);

        $component->assertStatus(200);
    }
}
