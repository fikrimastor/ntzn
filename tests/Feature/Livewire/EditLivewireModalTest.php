<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\EditLivewireModal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class EditLivewireModalTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(EditLivewireModal::class);

        $component->assertStatus(200);
    }
}
