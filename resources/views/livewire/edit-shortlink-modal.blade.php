<div x-on:close-modal.window="on = false" class="p-4">
    <div class="py-4">
        <form wire:submit.prevent="editShortlink">

            <div class="mb-2">
                <label class="block text-sm font-medium text-gray-700" for="code">
                    {{ __('CODE') }}
                </label>

                <input wire:model.debounce.300ms="code" type="text"
                    class="w-full py-2 pl-2 pr-4 mt-2 text-sm border border-gray-400 rounded-lg sm:text-base focus:outline-none focus:border-blue-400" />

                @error('code')
                <div class="ml-1 text-sm text-red-500">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div class="mb-2">
                <label class="block text-sm font-medium text-gray-700" for="info">
                    {{ __('INFO') }}
                </label>

                <input wire:model.debounce.300ms="info" type="text"
                    class="w-full py-2 pl-2 pr-4 mt-2 text-sm border border-gray-400 rounded-lg sm:text-base focus:outline-none focus:border-blue-400" />

                @error('info')
                <div class="ml-1 text-sm text-red-500">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div class="mb-2">
                <label class="block text-sm font-medium text-gray-700" for="url">
                    {{ __('URL') }}
                </label>

                <input wire:model.debounce.300ms="url" type="text"
                    class="w-full py-2 pl-2 pr-4 mt-2 text-sm border border-gray-400 rounded-lg sm:text-base focus:outline-none focus:border-blue-400" />

                @error('url')
                <div class="ml-1 text-sm text-red-500">
                    {{ $message }}
                </div>
                @enderror
            </div>

            {{-- @if (session('success'))
            <div class="px-4 py-3 mb-4 leading-normal text-blue-700 bg-blue-100 rounded-lg" role="alert">
                {{ session('success') }}
            </div>
            @endif --}}

            <div class="flex items-center mt-4">
                <button type="submit"
                    class="inline-flex items-center px-4 py-2 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-gray-800 border border-transparent rounded-md hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25">
                    {{ __('Edit Shortlink') }}
                </button>
            </div>
        </form>
    </div>
</div>
